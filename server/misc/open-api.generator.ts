import { UserController } from './../src/controllers-http/public/user.controller';
import { getMetadataArgsStorage } from "routing-controllers";
import { routingControllersToSpec } from 'routing-controllers-openapi'
import * as fs from "fs";

const publicControllers = [
    UserController
]

console.log(`Generating for ${publicControllers.length} controllers`);

const test = getMetadataArgsStorage();
const spec = routingControllersToSpec(test)

fs.writeFileSync(`${__dirname}/../openapi/openapi.json`, JSON.stringify(spec));