import { JWTService } from './../services/jwt.service';
import { Container } from 'typedi';
import { SpectrumCommand, receivedTextMessage, SpectrumLobby } from "spectrum-bot";
import { SpectrumConfirmCode } from "../entity/spectrum/confirm-code.entity";
import { MoreThan } from "typeorm";
import { User } from "../entity/user.entity";
import { SocketService } from '../services/socket.service';
import { SpectrumCodeClaimed } from "@prism/spectrum/state/spectrum.actions";
import { UserIdentity } from "@prism/core/interfaces/user-identity.interface";
import * as moment from 'moment';

@SpectrumCommand("auth (.*)$")
export class UseCodeCommand {

    protected _ws = Container.get(SocketService);
    protected _jwt = Container.get(JWTService);

    public async callback(message: receivedTextMessage, lobby: SpectrumLobby, matchs: string[]) {
        if (!matchs[1] || matchs[1].length !== 6) {
            return lobby.sendMessage(`Please input a valid 6 characters code`);
        }

        const regex = /auth (.*)$/;
        matchs = regex.exec(message.plaintext);
        const userCode = matchs[1];

        const code = await SpectrumConfirmCode.findOne({ where: { code: userCode, expires: MoreThan(moment().unix()) } });

        if (code) {
            // Check if we had an user for that handle before
            let user = await User.findOne({ where: { handle: code.handle } });
            if (!user) {
                // We didn't, simply create it.
                user = User.create({
                    handle: code.handle
                });
                await user.save();
            }
            await SpectrumConfirmCode.delete({ code: code.code });

            const jwt = this._jwt.sign<UserIdentity>({ userId: user.id, handle: user.handle });
            
            // tell the front it can login
            this._ws.server.to(`spectrumcode-${code.code}`).emit("actions", new SpectrumCodeClaimed(jwt));
            await lobby.sendMessage('Congrats ! Your RSI account is now authed with Prism.\nYou can now go back to the prism tab');
        } else {
            // This code doesn't exist
            await lobby.sendMessage(`Invalid code`);
        }
    }

}