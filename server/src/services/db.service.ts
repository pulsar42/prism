import { OauthRefreshToken } from './../entity/oauth/refresh-tokens.entity';
import { OauthClient } from './../entity/oauth/client.entity';
import { OauthAuthorizationCode } from './../entity/oauth/authorization-codes.entity';
import { OauthAccessToken } from './../entity/oauth/access-token.entity';
import { SpectrumConfirmCode } from './../entity/spectrum/confirm-code.entity';
import { User } from './../entity/user.entity';
import { Service } from "typedi";
import { createConnection, Connection } from "typeorm";
import { BehaviorSubject, Observable } from "rxjs";

@Service()
export class DbService {

    /** if we're connected to the db and ready to go */
    protected _ready: BehaviorSubject<boolean> = new BehaviorSubject(false);
    /** the current connection to the db*/
    protected _connection: Connection;

    /** if we're connected to the db and ready to go */
    public get ready(): Observable<boolean> { return this._ready.asObservable(); }

    constructor() {

        const options = {
            synchronize: true,
            logging: false,
            entities: [
                User,
                SpectrumConfirmCode,
                OauthAccessToken,
                OauthAuthorizationCode,
                OauthClient,
                OauthRefreshToken,
            ],
            migrations: [
                "src/migration/**/*.ts"
            ],
            subscribers: [
                "src/subscriber/**/*.ts"
            ],
        };
        createConnection({
            ...options,
            ...(process.env.NODE_ENV === "test" ? {
                type: "sqlite",
                database: ':memory:'
            } : {
                    type: "postgres",
                    host:  process.env.DB_HOST || "localhost",
                    port: 5432,
                    username: process.env.DB_USER || "test",
                    password: process.env.DB_PASSWORD || "test",
                    database: process.env.DB_NAME || "test",
                })
        }).then((c) => {
            this._connection = c;
            console.log(`DB connection ready`);
            this._ready.next(true);
        })
    }


    /**
     * Drops and recreates the db.
     * Only available in **test** environment
     */
    public async drop() {
        if (process.env.NODE_ENV === 'test' && this._connection) {
            return this._connection.synchronize(true);
        }
    }
}