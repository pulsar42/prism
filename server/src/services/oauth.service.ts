import { Container, Service } from "typedi";
import { ExpressService } from "./express.service";
import { SpectrumOauthModel } from "../oauth/spectrum.oauth";
import OAuthServer = require("express-oauth-server");
import { Request, Response } from "express";
import { ApiService } from "./api.service";

@Service()
export class OauthService {
    protected _express: ExpressService = Container.get(ExpressService);
    protected _api: ApiService = Container.get(ApiService);

    constructor() {
        this._express.app.oauth = new OAuthServer({
            model: new SpectrumOauthModel(),

        });

        this._express.app.get('/api/oauth2/authorize', this._express.app.oauth.authorize({
            allowEmptyState: true, authenticateHandler: {
                handle: async (req: Request, res: Response) => {
                    const jwt = req.headers['x-prism-auth'] || req.query['jwt'];
                    const user = await this._api.getCurrentJWTUser(jwt as string);

                    return { id: user.id };
                }
            }
        }));

        this._express.app.post('/api/oauth2/token', this._express.app.oauth.token());

        console.log(`Oauth Server ready`);
    }
}