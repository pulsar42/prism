import { UseCodeCommand } from './../spectrum/use-code.command';
import { BehaviorSubject } from 'rxjs';
import { Service } from "typedi";
import { Spectrum } from "spectrum-bot";
@Service()
export class SpectrumBot {
    protected _bot!: Spectrum;
    protected _ready: BehaviorSubject<boolean> = new BehaviorSubject(false);

    public get ready() { return this._ready.asObservable(); }

    constructor() {
        this._bot = new Spectrum();

        this._init();
    }

    protected async _init() {
        if(process.env.NODE_ENV != "test") {
            await this._bot.initAsUser(process.env.SPECTRUM_USERNAME, process.env.SPECTRUM_PASSWORD);
            await this._bot.getState().whenReady();
        }

        this._bot.getState().commands.setPrefix('!prism');

        await this._bot.getState().commands.registerCommands({
            commands: [
                UseCodeCommand
            ]
        });

        this._ready.next(true);
        console.log(`Spectrum ready`);
    }
}