import { Response } from 'express';
import { ClientPublicController } from './../controllers-http/client-public.controller';
import { User } from './../entity/user.entity';
import { UserIdentity } from '@prism/core/interfaces/user-identity.interface';
import { Service, Container } from 'typedi';
import { ExpressService } from './express.service';
import { useExpressServer, Action, HttpError } from 'routing-controllers';
import { SpectrumController } from '../controllers-http/spectrum.controller';
import { Request } from 'express';
import { JWTService } from './jwt.service';
import { UserController } from '../controllers-http/public/user.controller';
import { ClientController } from '../controllers-http/client.controller';
import { JsonWebTokenError } from 'jsonwebtoken';



@Service()
export class ApiService {
    protected _express: ExpressService = Container.get(ExpressService);
    protected _jwt: JWTService = Container.get(JWTService);

    constructor() {
        useExpressServer(this._express.app, {
            authorizationChecker: this._authorizationChecker.bind(this),
            currentUserChecker: this._getCurrentUser.bind(this),
            controllers: [SpectrumController, UserController, ClientController, ClientPublicController]
        });

        console.log(`API Ready`);
    }

    protected async _authorizationChecker(action: Action, roles: string[]): Promise<boolean> {
        try {
            // This route is accessible via _JWT (OR oauth)
            if (roles.find((r) => r === "_JWT")) {
                const auth = (action.request as Request).headers['x-prism-auth'];
                const jwtUser = await this.getCurrentJWTUser(auth as string);
                if (jwtUser) return true;
            }

            // At this point we've tried JWT and it didn't work,
            roles = roles.filter((r) => r !== "_JWT");

            /** @todo oauth flow */
        } catch (e) {
            console.error("ntm", e);
            if (e instanceof JsonWebTokenError) {
                switch (e.message) {
                    case "invalid signature":
                        throw new HttpError(401);
                }
            }

            console.error(e);
            return false;
        }
    }

    protected async _getCurrentUser(action: Action): Promise<User | null> {
        try {
            // Try with JWT first
            const jwtAuth = (action.request as Request).headers['x-prism-auth'];
            const jwtUser = await this.getCurrentJWTUser(jwtAuth as string);
            if (jwtUser) return jwtUser;

            // At this point it didn't work, so let's check if we have an oauth instead
            const auth = (action.request as Request).headers['authorization'];
            if (auth) {
                /** @todo oauth flow */
            }

        } catch (e) {
            console.error(e);
            return null;
        }
    }


    public async getCurrentJWTUser(jwt: string): Promise<User | undefined> {
        if (jwt) {
            const user = this._jwt.verify<UserIdentity>(jwt as string);

            if (user) {
                const dbUser = await User.findOne({ where: { id: user.userId, handle: user.handle } });
                return dbUser;
            }
        }
    }
}