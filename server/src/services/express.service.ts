import { Service } from "typedi";
import { Application } from "express";
import { Server } from "http";
import * as bodyParser from "body-parser";
import * as express from "express";
import * as cors from "cors";
import * as fs from "fs";
import OAuthServer = require('express-oauth-server');
@Service()
export class ExpressService {

    protected _app!: PrismExpress;
    protected _server!: Server;

    public get app() { return this._app; }
    public get server() { return this._server; }

    constructor() {
        this._app = express() as PrismExpress;
        this._app.use(cors({ origin: "*" }))
        this._app.use(bodyParser.json());
        this._app.use(bodyParser.urlencoded({ extended: false }));

        this._app.get('/openapi.json', async (_req, res) => {
            fs.readFile(`${__dirname}/../../openapi/openapi.json`, { encoding: "utf-8" }, (err, data) => {
                if (err) res.status(500);
                res.send(data);
            })
        });

        this._server = this._app.listen(3000);
        console.log(`Express ready`);
    }
}


export interface PrismExpress extends Application {
    oauth?: OAuthServer
}