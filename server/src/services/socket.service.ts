import { Container, Service } from "typedi";
import { ExpressService } from "./express.service";
import { useSocketServer } from "socket-controllers";
import * as SocketIO from "socket.io";

@Service()
export class SocketService {
    protected _express: ExpressService = Container.get(ExpressService);
    protected _server!: SocketIO.Server;

    public get server() { return this._server; }

    constructor() {
        this._server = SocketIO(this._express.server);
        useSocketServer(this._server, {
            controllers: [
                __dirname + "/../controllers-ws/**.controller.ws.ts",
                __dirname + "/../controllers-ws/**.controller.ws.js"
            ]
        });
        console.log("Socket server ok");
    }
}