import * as jwt from "jsonwebtoken";
import { Service } from "typedi";

@Service()
export class JWTService {
    constructor() { }

    public sign<T = string | Buffer | Object>(data: T): string {
        return jwt.sign(data as any, process.env.JWT_SECRET);
    }

    /**
     * verify a JWT token
     * 
     * @todo invalidation & refresh strat
     * @param jwtToken the token to verify
     */
    public verify<T = any>(jwtToken: string): T {
        return jwt.verify(jwtToken, process.env.JWT_SECRET) as unknown as T;
    }

}