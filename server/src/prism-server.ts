import { ApiService } from './services/api.service';
import { SocketService } from './services/socket.service';
import { OauthService } from './services/oauth.service';
import { Container } from 'typedi';
import { DbService } from './services/db.service';
import { SpectrumBot } from './services/spectrum-bot.service';
export class PrismServer {
    constructor() {
        Container.get(DbService);
        Container.get(OauthService);
        Container.get(SocketService);
        Container.get(ApiService);
        Container.get(SpectrumBot)
        console.log(`Server ready`);
    }
}