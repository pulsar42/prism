import { OauthAccessToken } from './../entity/oauth/access-token.entity';
import { OauthAuthorizationCode } from './../entity/oauth/authorization-codes.entity';
import { OauthClient } from './../entity/oauth/client.entity';
import { User as UserEntity } from './../entity/user.entity';
import { OauthRefreshToken } from './../entity/oauth/refresh-tokens.entity';
import { RefreshTokenModel, AuthorizationCodeModel, RefreshToken, AuthorizationCode, Client, User, Token } from "oauth2-server";
import * as moment from "moment";
export class SpectrumOauthModel implements AuthorizationCodeModel, RefreshTokenModel {

    public async getRefreshToken(refreshToken: string): Promise<RefreshToken | null> {
        const refresh = await OauthRefreshToken.findOne({ where: { refresh_token: refreshToken } });

        return {
            refreshToken: refresh.refresh_token,
            refreshTokenExpiresAt: moment.unix(refresh.expires).toDate(),
            scope: refresh.scopes,
            client: this._mapClient(refresh.client),
            user: this._mapUser(refresh.user)
        }
    }

    public async getAuthorizationCode(authorizationCode: string): Promise<AuthorizationCode> {
        const code = await OauthAuthorizationCode.findOne({ where: { authorization_code: authorizationCode }, relations: ['client', 'user'] });

        return {
            authorizationCode: code.authorization_code,
            expiresAt: moment.unix(code.expires).toDate(),
            redirectUri: code.redirect_uri,
            scope: code.scopes,
            client: this._mapClient(code.client),
            user: this._mapUser(code.user)
        }
    }

    public async saveAuthorizationCode(code: AuthorizationCode, client: Client, user: User): Promise<AuthorizationCode> {

        const dbCode = new OauthAuthorizationCode();

        dbCode.authorization_code = code.authorizationCode;
        dbCode.expires = moment().add(5, "minutes").unix();
        dbCode.redirect_uri = code.redirectUri;
        dbCode.scopes = code.scope as string[];
        dbCode.client = await OauthClient.findOne({ where: { client_id: client.id } });
        dbCode.user = await UserEntity.findOne({ where: { id: user.id } });

        await dbCode.save();

        return {
            ...code,
            expiresAt: moment.unix(dbCode.expires).toDate(),
            user: this._mapUser(dbCode.user)
        }
    }

    public async revokeAuthorizationCode(code: AuthorizationCode): Promise<boolean> {
        await OauthAuthorizationCode.delete({ authorization_code: code.authorizationCode });

        return true;
    }

    public async revokeToken(token: RefreshToken | Token): Promise<boolean> {
        if (token && (token as Token).accessToken) {
            await OauthAccessToken.delete({ access_token: (token as Token).accessToken });
            return true;
        } else if (token && (token as RefreshToken).refreshToken) {
            await OauthRefreshToken.delete({ refresh_token: (token as RefreshToken).refreshToken });
            return true;
        }

        return false;
    }

    public async getClient(clientId: string, clientSecret?: string): Promise<Client> {

        if (clientSecret) {
            return this._mapClient(await OauthClient.findOne({ where: { client_secret: clientSecret, client_id: clientId } }));
        } else {
            return this._mapClient(await OauthClient.findOne({ where: { client_id: clientId } }));
        }

    }

    public async saveToken(token: Token, client: Client, user: User): Promise<Token> {
        const oauthToken = new OauthAccessToken();
        const refreshToken = new OauthRefreshToken();

        const dbClient = await OauthClient.findOne({ where: { id: client.id } });
        const dbUser = await UserEntity.findOne({ where: { id: user.id } });
        const scopes = typeof token.scope === "string" ? [token.scope] : token.scope;

        oauthToken.access_token = token.accessToken;
        oauthToken.expires = moment(token.accessTokenExpiresAt).unix();
        oauthToken.scopes = scopes;
        oauthToken.user = dbUser
        oauthToken.client = dbClient

        refreshToken.refresh_token = token.refreshToken;
        refreshToken.expires = moment(token.refreshTokenExpiresAt).unix();
        refreshToken.client = dbClient;
        refreshToken.user = dbUser;
        refreshToken.scopes = scopes;

        await oauthToken.save();
        await refreshToken.save();

        return { ...token, user: this._mapUser(dbUser), client: this._mapClient(dbClient) };

    }

    public async getAccessToken(accessToken: string): Promise<Token> {
        const token = await OauthAccessToken.findOne({ where: { access_token: accessToken } });
        if (!token) return null;

        return {
            accessToken: token.access_token,
            accessTokenExpiresAt: moment.unix(token.expires).toDate(),
            client: this._mapClient(token.client),
            user: this._mapUser(token.user),
        }
    }

    public async verifyScope(token: Token, scope: string | string[]): Promise<boolean> {
        const scopesInToken = typeof token.scope === "string" ? [token.scope] : token.scope;
        const scopesWanted = typeof scope === "string" ? [scope] : scope;

        if (!Array.isArray(scopesInToken) || !Array.isArray(scopesWanted) || scopesInToken.length !== scopesWanted.length)
            return false;

        const arr1 = scopesInToken.concat().sort();
        const arr2 = scopesWanted.concat().sort();

        for (let i = 0; i < arr1.length; i++) {

            if (arr1[i] !== arr2[i])
                return false;

        }

        return true;
    }



    protected _mapClient(client: OauthClient): Client {
        return {
            id: client.client_id,
            redirectUris: client.redirect_uri,
            /** @todo grants */
            grants: ["authorization_code", "refresh_token"],
            accessTokenLifetime: 60 * 60 * 24 * 7,
            refreshTokenLifetime: 60 * 60 * 24 * 31 * 6,
        }
    }

    protected _mapUser(user: UserEntity): User {
        return {
            id: user.id
        }
    }

}