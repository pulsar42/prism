import { SocketController, OnMessage, ConnectedSocket, MessageBody } from "socket-controllers";
import { Socket } from "socket.io";
import { SpectrumListenForCode } from "@prism/spectrum/state/spectrum.actions";


@SocketController()
export class SpectrumControllerWS {
    /**
     * Connect the given socket to a room in which he'll get updates on a given code
     */
    @OnMessage(SpectrumListenForCode.type)
    public async listenForCode(@ConnectedSocket() socket: Socket, @MessageBody() body: SpectrumListenForCode) {
        const codeRooms = Object.keys(socket.rooms).filter(roomId => roomId && roomId.startsWith('spectrumcode-'));
        await Promise.all(
            codeRooms.map(async (roomId) => {
                // This is a spectrumcode room but not with the code we're listening for
                if (!roomId.endsWith(body.code)) {
                    // So we leave it
                    await new Promise((resolve, reject) => socket.leave(roomId, (err?) => err ? resolve() : reject(err)))
                }
            })
        );

        // And join the room for the given spectrum code
        await new Promise((resolve, reject) => socket.join(`spectrumcode-${body.code}`, (err?) => err ? resolve() : reject(err)));
    }
}