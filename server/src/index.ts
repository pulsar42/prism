require('dotenv').config()
// www.js, index.js, main.js, etc
const TSModuleAlias = require("@momothepug/tsmodule-alias");
// Makes it work with playAuto method
// this method Will scan backward until tsconfig is found
const aliasRegister = TSModuleAlias.playAuto(__dirname);

import { PrismServer } from "./prism-server";
new PrismServer();