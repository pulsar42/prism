import { Entity, BaseEntity, PrimaryColumn, Column, Index } from "typeorm";

@Entity()
export class SpectrumConfirmCode extends BaseEntity {
    @PrimaryColumn()
    code: string;

    @Column({ nullable: false })
    @Index({ unique: true })
    handle: string;

    @Column({ nullable: false, type: "numeric" })
    expires: number;
}