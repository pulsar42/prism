import { User } from './../user.entity';
import { Entity, Column, CreateDateColumn, BaseEntity, PrimaryColumn, OneToOne, ManyToOne } from "typeorm";
import { OauthClient } from './client.entity';

@Entity()
export class OauthAccessToken extends BaseEntity {
    @PrimaryColumn()
    access_token: string;

    @CreateDateColumn()
    createdAt: Date;

    @ManyToOne(() => User)
    user: User;

    @ManyToOne(() => OauthClient)
    client: OauthClient;

    /** unix timestamp */
    @Column({ nullable: false })
    expires: number;

    @Column({ type: "simple-array" })
    scopes: string[]
}
