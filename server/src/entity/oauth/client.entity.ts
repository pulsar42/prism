import { User } from './../user.entity';
import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, Index, BaseEntity, PrimaryColumn, TableForeignKey, OneToOne, ManyToOne } from "typeorm";

@Entity()
export class OauthClient extends BaseEntity {
    @PrimaryColumn({ nullable: false })
    client_id: string;

    @Column({ nullable: false })
    client_secret: string;

    @Column({ nullable: false })
    name: string;

    @Column({ nullable: true })
    logo: string;

    @Column({ length: 400, nullable: true })
    description: string;

    @Column({ type: "simple-array", nullable: true })
    redirect_uri: string[];

    @Column({ type: "simple-array", nullable: true })
    grant_types: string[];

    @Column({ type: "simple-array", nullable: true })
    scope: string[];

    @ManyToOne(() => User, { nullable: false })
    owner: User;
}
