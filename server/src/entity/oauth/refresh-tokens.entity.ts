import { OauthClient } from './client.entity';
import { User } from './../user.entity';
import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, Index, BaseEntity, PrimaryColumn, TableForeignKey, OneToOne, ManyToOne } from "typeorm";

@Entity()
export class OauthRefreshToken extends BaseEntity {
    @PrimaryColumn()
    refresh_token: string;

    @CreateDateColumn()
    createdAt: Date;

    @ManyToOne(() => User)
    user: User;

    @ManyToOne(() => OauthClient)
    client: OauthClient;
    
    /** unix timestamp */
    @Column({ nullable: false })
    expires: number;

    @Column({ type: "simple-array" })
    scopes: string[]
}
