import { User } from './../user.entity';
import { Entity, Column, BaseEntity, PrimaryColumn, OneToOne, ManyToOne } from "typeorm";
import { OauthClient } from './client.entity';

@Entity()
export class OauthAuthorizationCode extends BaseEntity {
    @PrimaryColumn()
    authorization_code: string;

    /** unix timestamp */
    @Column({ nullable: false })
    expires: number;

    @Column()
    redirect_uri: string

    @Column({ type: "simple-array" })
    scopes: string[]

    @ManyToOne(() => User)
    user: User;

    @ManyToOne(() => OauthClient)
    client: OauthClient;
}
