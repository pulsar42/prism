import { SpectrumUser } from './../interfaces/spectrum-user.interface';
import { User } from './../../entity/user.entity';
import { SpectrumConfirmCode } from '../../entity/spectrum/confirm-code.entity';
import { JsonController, Get, Post, BodyParam, Authorized, CurrentUser } from "routing-controllers";
import { OpenAPI } from 'routing-controllers-openapi'
import * as randomstring from "randomstring";
import * as moment from "moment";

@JsonController("/api/v1/users")
export class UserController {
    constructor() { }

    /**
     * Get informations on the user doing this request
     * @param currentUser 
     */
    @Get("/me")
    @Authorized(['_JWT', 'identify'])
    @OpenAPI({ summary: 'Get basic informations on the user' })
    public me(@CurrentUser({ required: true }) currentUser: User): SpectrumUser {
        return { handle: currentUser.handle };
    }
}