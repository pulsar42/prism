import { SpectrumConfirmCode } from './../entity/spectrum/confirm-code.entity';
import { JsonController, Get, Post, BodyParam } from "routing-controllers";
import * as randomstring from "randomstring";
import * as moment from "moment";

@JsonController("/api/v1/spectrum")
export class SpectrumController {
    constructor() { }

    /**
     * Called by the front when an unregistered user tries to claim an handle.
     * Will generate a temp token to be used to authentify on spectrum
     * 
     * @param handle the handle we're trying to claim
     */
    @Post("/code/generate")
    public async generateCode(@BodyParam("handle") handle: string) {
        handle = handle.toLowerCase();
        // Find an existing code
        let code = await SpectrumConfirmCode.findOne({ where: { handle: handle } });

        // We have a code but it's expired, so we dump it
        if (code && moment.unix(Number(code.expires)).isSameOrBefore(moment(), "second")) {
            await code.remove();
            code = null;
        }

        // We don't have a code, so we create one
        if (!code) {
            code = new SpectrumConfirmCode();
            code.code = randomstring.generate({ readable: true, length: 6 });
            code.handle = handle;
            code.expires = moment().add(10, "minutes").unix();
            await code.save();
        }


        return code;
    }
}