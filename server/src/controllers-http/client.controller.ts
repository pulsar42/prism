import { OauthClient } from './../entity/oauth/client.entity';
import { User } from './../entity/user.entity';
import { JsonController, Get, Authorized, CurrentUser, Post, BodyParam, Param, Patch, Body } from "routing-controllers";
import * as randomstring from "randomstring";

@JsonController("/api/v1/clients")
export class ClientController {
    constructor() { }

    @Get("/")
    @Authorized(['_JWT'])
    public async myClients(@CurrentUser({ required: true }) currentUser: User): Promise<OauthClient[]> {
        return OauthClient.find({ where: { owner: { id: currentUser.id } } });
    }



    @Post('/')
    @Authorized(['_JWT'])
    public async createClient(@CurrentUser({ required: true }) currentUser: User, @BodyParam("clientName") clientName: string): Promise<OauthClient> {
        const client = new OauthClient();

        client.name = clientName;
        client.client_id = randomstring.generate({ length: 18, charset: "numeric" });
        client.client_secret = randomstring.generate({ length: 32 });
        client.owner = currentUser;

        await client.save();

        return client;
    }

    @Get("/:id")
    @Authorized(['_JWT'])
    public async getDevClient(@CurrentUser({ required: true }) currentUser: User, @Param("id") id: string): Promise<OauthClient> {
        return OauthClient.findOne({ where: { client_id: id, owner: { id: currentUser.id } } });
    }

    @Patch('/:id')
    @Authorized(['_JWT'])
    public async patchClient(@CurrentUser({ required: true }) currentUser: User, @Param("id") id: string, @Body() patchClient: { name: string, description?: string, logo?: string, redirect_uris?: string[] }): Promise<OauthClient> {
        const client = await OauthClient.findOne({ where: { client_id: id, owner: { id: currentUser.id } } });

        if (!patchClient.name || patchClient.name.length < 4) throw new Error(`Name required`);
        if (patchClient.logo && !new RegExp(/^https:\/\/.*[^\/]{1,}(\.jpg|\.png)$/).test(patchClient.logo)) throw new Error(`Invalid logo URL`);
        

        client.logo = patchClient.logo;
        client.name = patchClient.name;
        client.description = patchClient.description;
        client.redirect_uri = patchClient.redirect_uris;

        await client.save();

        return client;
    }

}