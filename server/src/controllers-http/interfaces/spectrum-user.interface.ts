export interface SpectrumUser {
    handle: string;
    community_monicker?: string;
    avatar?: string;
}