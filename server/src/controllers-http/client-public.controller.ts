import { OauthClient } from '../entity/oauth/client.entity';
import { User } from '../entity/user.entity';
import { JsonController, Get, Authorized, CurrentUser, Post, BodyParam, Param, Patch, Body } from "routing-controllers";
import * as randomstring from "randomstring";

@JsonController("/api/v1/clients/public")
export class ClientPublicController {
    constructor() { }

    @Get("/:id")
    @Authorized(['_JWT'])
    public async getPublicClient(@CurrentUser({ required: true }) currentUser: User, @Param("id") id: string) {
        const client = await OauthClient.findOne({ where: { client_id: id } });

        if (client) {
            return {
                client_id: client.client_id,
                name: client.name,
                description: client.description,
                logo: client.logo
            }
        } else return null;
    }

}