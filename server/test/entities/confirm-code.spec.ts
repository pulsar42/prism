import { SpectrumConfirmCode } from './../../src/entity/spectrum/confirm-code.entity';
import { DbService } from './../../src/services/db.service';
import { Container } from 'typedi';
import { filter, take } from "rxjs/operators";
import * as should from "should";
import * as moment from "moment";

describe("confirm-code.entity.ts", () => {

    before(async () => {
        await Container.get(DbService).ready.pipe(filter((v) => v), take(1)).toPromise();
    })

    after(async () => {
        await Container.get(DbService).drop();
    })

    it(`Should not allow duplicates code for the same handle`, async () => {
        const test = await SpectrumConfirmCode.insert({ code: "abc", handle: "123", expires: moment().unix() })
        const test2 = SpectrumConfirmCode.insert({ code: "abcd", handle: "123", expires: moment().unix() })

        return test2.should.be.rejected();
    })
})