import { SocketService } from './../../src/services/socket.service';
import { SpectrumListenForCode } from '@prism/spectrum/state/spectrum.actions';
import { receivedTextMessage } from 'spectrum-bot';
import { UseCodeCommand } from './../../src/spectrum/use-code.command';
import { User } from './../../src/entity/user.entity';
import { SpectrumLobby } from 'spectrum-bot';
import { SpectrumController } from './../../src/controllers-http/spectrum.controller';
import * as should from "should";
import { SpectrumConfirmCode } from '../../src/entity/spectrum/confirm-code.entity';
import sinon = require('sinon');
import * as moment from "moment";
import * as socketClient from "socket.io-client"
import Container from 'typedi';

describe("spectrum", () => {
    const controller = new SpectrumController();

    describe("Authing Mechanism", () => {

        describe("Generating a code", () => {

            afterEach(async () => {
                await SpectrumConfirmCode.clear();
            })

            it('Should generate a six characters random code', async () => {
                const code = await controller.generateCode("test");
                code.code.length.should.be.eql(6);
            });

            it('Should generate a code valid for 10 minutes', async () => {
                const code = await controller.generateCode("test");
                code.expires.should.be.approximately(moment().add(10, "minutes").unix(), 60);
            });

            it('Should generate a code linked to the specified handle', async () => {
                const code = await controller.generateCode("test");
                code.handle.should.be.eql("test");
            });
        })

        describe("Using a code", () => {
            let code: SpectrumConfirmCode;
            const command: UseCodeCommand = new UseCodeCommand();

            beforeEach(async () => {
                code = await controller.generateCode("abcdef");
            });

            afterEach(async () => {
                await SpectrumConfirmCode.clear();
                await User.clear();
            })

            it('Should inform the user on invalid code format', async () => {
                const stub = sinon.createStubInstance(SpectrumLobby);
                command.callback({ plaintext: "" } as receivedTextMessage, stub as unknown as SpectrumLobby, []);
                command.callback({ plaintext: "auth aze" } as receivedTextMessage, stub as unknown as SpectrumLobby, ["auth aze", "aze"]);
                command.callback({ plaintext: "auth azedfeze"} as receivedTextMessage, stub as unknown as SpectrumLobby, ["auth azedfeze", "azedfeze"]);
                stub.sendMessage.alwaysCalledWith('Please input a valid 6 characters code' as any).should.be.true("informed the user");
            });

            it('Should inform the user on expired code', async () => {
                const stab = sinon.createStubInstance(SpectrumLobby);
                const clock = sinon.useFakeTimers(moment().add(15, "minutes").toDate());
                await command.callback({ plaintext: "auth abcdef" } as receivedTextMessage, stab as unknown as SpectrumLobby, ["auth abcdef", code.code]);
                stab.sendMessage.calledOnceWith('Invalid code' as any).should.be.true("informed the user");
                clock.restore();
            });

            it("Should create an user with the linked handle", async () => {
                const stub = sinon.createStubInstance(SpectrumLobby);
                await command.callback({ plaintext: "auth abcdef" } as receivedTextMessage, stub as unknown as SpectrumLobby, ["auth abcdef", code.code]);

                const user = await User.findOne({ where: { handle: "abcdef" } });

                user.should.be.ok();
            });

            it("Should remove successfully used code", async () => {
                const stub = sinon.createStubInstance(SpectrumLobby);
                await command.callback({} as receivedTextMessage, stub as unknown as SpectrumLobby, ["auth abcdef", code.code]);

                const noCode = await SpectrumConfirmCode.findOne({ where: { handle: "abcdef" } });
                should(noCode).be.undefined();
            });

            it("Should return existing user");
            it("Should dispatch a socket event to notify the front-end")
        })

        // describe(`Waiting for a code to be used`, () => {
        //     let socket: SocketIOClient.Socket;
        //     before(async function() {
        //         this.timeout(5000);
        //         return new Promise((resolve, reject) => {
        //             socket = socketClient("http://localhost")
        //             socket.on('connect', () => { resolve() });

        //             setTimeout(() => reject("timeout"), 5000);
        //         });
        //     });

        //     after(async () => {
        //         socket.disconnect();
        //     });


        //     it(`Should join a room for the given code`, async () => {
        //         socket.emit(SpectrumListenForCode.type, new SpectrumListenForCode("abcdef"));

        //         Container.get(SocketService).server.sockets.sockets[socket.id].rooms[`spectrumcode-abcdef`].should.be.ok();
        //     });


        //     it(`Should not be allowed to join rooms for different codes`)
        // })

    })

})