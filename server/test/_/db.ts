import Container from "typedi";
import { DbService } from "../../src/services/db.service";
import { filter, take } from "rxjs/operators";
import * as should from "should";
// www.js, index.js, main.js, etc
const TSModuleAlias = require("@momothepug/tsmodule-alias");
// Makes it work with playAuto method
// this method Will scan backward until tsconfig is found
const aliasRegister = TSModuleAlias.playAuto(__dirname);

describe("_", () => {

    describe('Database', () => {
        before(async function () {
            this.timeout(10000);
            const db = Container.get(DbService);
            await db.ready.pipe(filter((v) => v), take(1)).toPromise();
        })

        it('Should have connected', async () => {
            const test = await Container.get(DbService).ready.pipe(take(1)).toPromise();
            test.should.be.equal(true);
        });
    })

})
