# Auth0

OAuth2 enables application developers to build applications that utilize authentication and data from the Prism API. It is the main feature of Prism Auth. We currently **only** support the authorization code grant.

## Shared Ressources
The first step in implementing OAuth2 <a href="en/dashboard/view/applications">is registering a developer application</a> and retrieving your client ID and client secret. Most people who will be implementing OAuth2 will want to find and utilize a [library in the language of their choice](https://oauth.net/code/). For those implementing OAuth2 from scratch, please see [RFC 6749](https://tools.ietf.org/html/rfc6749) for details. After you create your application, make sure that you have your `client_id` and `client_secret` handy.



###### OAuth2 URLs

| URL | Description |
|-----|-------------|
| https://api.prism.pulsar42.sc/api/oauth2/authorize | Base authorization URL |
| https://api.prism.pulsar42.sc/api/oauth2/token | Token URL |

<!-- | https://discordapp.com/api/oauth2/token/revoke | Revocation URL | -->


###### OAuth2 Scopes

| Name | Description |
|-----|-------------|
| Identify | Base scope |

