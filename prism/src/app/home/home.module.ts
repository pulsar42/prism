import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './components/home/home.component';
import { RouterModule } from '@angular/router';
import { MatButtonModule, MatCardModule, MatIconModule, MatStepperModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    declarations: [HomeComponent],
    imports: [
        CommonModule,
        MatButtonModule,
        MatCardModule,
        MatIconModule,
        MatStepperModule,
        MatFormFieldModule,
        MatInputModule,
        TranslateModule,
        RouterModule.forChild([
            { path: '', pathMatch: 'full', component: HomeComponent }
        ])
    ]
})
export class HomeModule { }
