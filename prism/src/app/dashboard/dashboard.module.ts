import { MarkdownModule } from 'ngx-markdown';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import {
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatSlideToggleModule,
    MatSidenavModule,
    MatTabsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatDivider,
    MatDividerModule
} from '@angular/material';
import { LocalizeRouterModule } from '@gilsdav/ngx-translate-router';
import { ApplicationsComponent } from './components/applications/applications.component';
import { MainViewComponent } from './components/main-view/main-view.component';
import { SidenavApplicationsComponent } from './components/sidenav/sidenav-applications/sidenav-applications.component';
import { SidenavMainComponent } from './components/sidenav/sidenav-main/sidenav-main.component';
import { LayoutComponent } from './components/layout/layout.component';
import { CreateApplicationPopupComponent } from './components/applications/create-application-popup/create-application-popup.component';
import { NgxsModule } from '@ngxs/store';
import { DashboardState } from './state/dashboard.state';
import { ViewApplicationComponent } from './components/view-application/view-application.component';
import { DocumentationComponent } from './components/documentation/documentation.component';

@NgModule({
    declarations: [
        DashboardComponent,
        ApplicationsComponent,
        MainViewComponent,
        SidenavApplicationsComponent,
        SidenavMainComponent,
        LayoutComponent,
        CreateApplicationPopupComponent,
        ViewApplicationComponent,
        DocumentationComponent,
    ],
    entryComponents: [CreateApplicationPopupComponent],
    imports: [
        FormsModule,
        NgxsModule.forFeature([DashboardState]),
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        CommonModule,
        MatToolbarModule,
        MatIconModule,
        MatButtonModule,
        MatSlideToggleModule,
        DashboardRoutingModule,
        MatSidenavModule,
        MatTabsModule,
        MatDividerModule,
        MatDialogModule,
        LocalizeRouterModule,
        MarkdownModule.forChild()
    ]
})
export class DashboardModule { }
