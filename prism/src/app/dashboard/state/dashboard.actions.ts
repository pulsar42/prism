export class FetchDevClients {
    static readonly type = '[Dashboard] Fetch dev clients';
    constructor() { }
}

export class FetchDevClient {
    static readonly type = '[Dashboard] Fetch dev client';
    constructor(public clientId: string, public force: boolean = false) { }
}

export class CreateDevClient {
    static readonly type = '[Dashboard] Create dev client';
    constructor(public clientName: string) { }
}


export class UpdateDevClient {
    static readonly type = '[Dashboard] Update dev client';
    constructor(public client: { client_id: string, name: string, description: string, logo: string }) { }
}
