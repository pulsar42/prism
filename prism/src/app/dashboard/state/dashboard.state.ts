import { State, Action, StateContext, Selector } from '@ngxs/store';
import { OauthDevClient } from '../interfaces/oauth-dev-client.interface';
import { FetchDevClients, CreateDevClient, FetchDevClient, UpdateDevClient } from './dashboard.actions';
import { ApiService } from '../../core/services/api.service';
import { map } from 'rxjs/operators';

export class DashboardStateModel {
    public devClients: OauthDevClient[];
}

@State<DashboardStateModel>({
    name: 'dashboard',
    defaults: {
        devClients: []
    }
})
export class DashboardState {

    constructor(
        protected _api: ApiService
    ) { }

    /**
     * Get a specific dev client by id
     */
    @Selector()
    static devClientById(state: DashboardStateModel) {
        return (id: string) => state.devClients.find((client) => client.client_id === id);
    }

    @Action(FetchDevClient)
    public fetchDevClientById(ctx: StateContext<DashboardStateModel>, action: FetchDevClient) {
        if (action.force || !ctx.getState().devClients.find((c) => c.client_id === action.clientId)) {
            return this._api.get<OauthDevClient>(`clients/${action.clientId}`)
                .pipe(
                    map((client) => ctx.patchState({ devClients: [...ctx.getState().devClients, client] }))
                );
        }
    }

    @Action(CreateDevClient)
    public createDevClient(ctx: StateContext<DashboardStateModel>, action: CreateDevClient) {
        return this._api.post<OauthDevClient>(`clients/`, { clientName: action.clientName });
    }

    @Action(FetchDevClients)
    public fetchDevClients(ctx: StateContext<DashboardStateModel>, action: FetchDevClients) {
        return this._api.get<OauthDevClient[]>(`clients/`).pipe(map((clients) => ctx.patchState({ devClients: clients })));
    }


    @Action(UpdateDevClient)
    public updateDevClieent(ctx: StateContext<DashboardStateModel>, action: UpdateDevClient) {
        return this._api.patch<OauthDevClient>(`clients/${action.client.client_id}`, action.client)
            .pipe(
                map((client) => {
                    const clients = ctx.getState().devClients;
                    clients.map((c) => {
                        if (c.client_id !== client.client_id) { return c; } else { return client; }
                    });

                    ctx.patchState({ devClients: clients });
                })
            );
    }
}
