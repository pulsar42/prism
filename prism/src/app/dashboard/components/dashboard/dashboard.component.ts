import { GetIdentity } from './../../../core/state/core.actions';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';

@Component({
    selector: 'prism-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

    public sidenav: boolean = true;

    public links = [
        { label: 'Dashboard', link: ['/dashboard/view/main'], devMode: true },
        { label: 'Applications', link: ['/dashboard/view/applications'], devMode: true },
        { label: 'Documentation', link: ['/dashboard/view/documentation'], devMode: true }
    ];

    constructor(
        protected _store: Store
    ) {
        this._store.dispatch(new GetIdentity());
    }

    ngOnInit() {
    }

}
