import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateApplicationPopupComponent } from './create-application-popup.component';

describe('CreateApplicationPopupComponent', () => {
  let component: CreateApplicationPopupComponent;
  let fixture: ComponentFixture<CreateApplicationPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateApplicationPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateApplicationPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
