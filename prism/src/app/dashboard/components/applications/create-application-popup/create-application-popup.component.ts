import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
    selector: 'prism-create-application-popup',
    templateUrl: './create-application-popup.component.html',
    styleUrls: ['./create-application-popup.component.scss']
})
export class CreateApplicationPopupComponent {

    public applicationName: string = '';

    constructor(
        public dialogRef: MatDialogRef<CreateApplicationPopupComponent>) { }

    onNoClick(): void {
        this.dialogRef.close();
    }


}
