import { FeatureState } from './../../../core/state/index';
import { FetchDevClients } from './../../state/dashboard.actions';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { CreateApplicationPopupComponent } from './create-application-popup/create-application-popup.component';
import { Store, Select } from '@ngxs/store';
import { CreateDevClient } from '../../state/dashboard.actions';
import { OauthDevClient } from '../../interfaces/oauth-dev-client.interface';
import { Observable } from 'rxjs';

@Component({
    selector: 'prism-applications',
    templateUrl: './applications.component.html',
    styleUrls: ['./applications.component.scss']
})
export class ApplicationsComponent implements OnInit {

    @Select((state: FeatureState) => state.dashboard.devClients)
    public devClients$: Observable<OauthDevClient[]>;

    constructor(
        protected _dialog: MatDialog,
        protected _store: Store
    ) {
        this._store.dispatch(new FetchDevClients());
    }

    ngOnInit() {
    }

    public popupCreateApplication() {
        this._dialog.open(CreateApplicationPopupComponent).afterClosed().subscribe((data) => {
            if (data) {
                this._store.dispatch(new CreateDevClient(data));
            }
        });
    }
}
