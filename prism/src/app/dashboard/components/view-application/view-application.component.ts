import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { DashboardState } from '../../state/dashboard.state';
import { OauthDevClient } from '../../interfaces/oauth-dev-client.interface';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { map, tap } from 'rxjs/operators';
import { FetchDevClient, UpdateDevClient } from '../../state/dashboard.actions';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';

@Component({
    selector: 'prism-view-application',
    templateUrl: './view-application.component.html',
    styleUrls: ['./view-application.component.scss']
})
export class ViewApplicationComponent implements OnInit {

    public client$: Observable<OauthDevClient>;

    public formGroup!: FormGroup;

    constructor(
        protected _store: Store,
        protected _activatedRoute: ActivatedRoute
    ) {
        this._activatedRoute.params.subscribe((params: { applicationId: string | null }) => {
            if (params.applicationId) {
                this._store.dispatch(new FetchDevClient(params.applicationId));
                this.client$ = this._store.select(DashboardState.devClientById).pipe(
                    map(selector => selector(params.applicationId)),
                    tap((client) => {
                        if (client) {
                            this.formGroup = new FormGroup({
                                client_id: new FormControl(client.client_id, Validators.required),
                                name: new FormControl(client.name, [Validators.required, Validators.minLength(4)]),
                                logo: new FormControl(client.logo, [
                                    Validators.pattern(/^https:\/\/.*[^\/]{1,}(\.jpg|\.png)$/),
                                ]
                                ),
                                redirect_uris: new FormArray(
                                    client.redirect_uri ? client.redirect_uri.map((url) => this._genUriControl(url)) : []
                                ),
                                description: new FormControl(client.description, Validators.maxLength(400))
                            });
                        }
                    })
                );
            }
        });

    }

    public update() {
        this._store.dispatch(new UpdateDevClient({
            ...this.formGroup.value,
        }));
    }

    public addUri() {
        (this.formGroup.get('redirect_uris') as FormArray).push(this._genUriControl());
    }

    public removeUri(i: number) {
        (this.formGroup.get('redirect_uris') as FormArray).removeAt(i);
    }

    protected _genUriControl(init: string = '') {
        return new FormControl(init, [Validators.required, Validators.pattern(/^https|http:\/\/.*$/)])
    }

    ngOnInit() {
    }

}
