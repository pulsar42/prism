import { ActivatedRoute } from '@angular/router';
import { FeatureState } from './../../../core/state/index';
import { FetchDevClients } from './../../state/dashboard.actions';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Store, Select } from '@ngxs/store';
import { CreateDevClient } from '../../state/dashboard.actions';
import { OauthDevClient } from '../../interfaces/oauth-dev-client.interface';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MarkdownService } from 'ngx-markdown';

@Component({
    selector: 'prism-documentation',
    templateUrl: './documentation.component.html',
    styleUrls: ['./documentation.component.scss']
})
export class DocumentationComponent implements OnInit {
    public currentDoc$: Observable<string>;

    constructor(
        protected _dialog: MatDialog,
        protected _store: Store,
        protected _activatedRoute: ActivatedRoute,
        protected _markdown: MarkdownService
    ) {
        this.currentDoc$ = this._activatedRoute.paramMap.pipe(map(params => params.get('doc')));
    }

    ngOnInit() {
    }

}
