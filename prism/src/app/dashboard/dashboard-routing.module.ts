import { ApplicationsComponent } from './components/applications/applications.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { MainViewComponent } from './components/main-view/main-view.component';
import { SidenavMainComponent } from './components/sidenav/sidenav-main/sidenav-main.component';
import { ViewApplicationComponent } from './components/view-application/view-application.component';
import { DocumentationComponent } from './components/documentation/documentation.component';

const routes: Routes = [
    {
        path: '', redirectTo: 'view', pathMatch: 'full',
    },
    {
        path: 'view', component: DashboardComponent,
        children: [
            { path: '', redirectTo: 'main', pathMatch: 'full' },
            { path: 'main', component: MainViewComponent },
            { path: 'documentation', redirectTo: 'documentation/intro' },
            { path: 'documentation/:doc', component: DocumentationComponent },
            { path: 'applications', component: ApplicationsComponent },
            { path: 'applications/:applicationId', component: ViewApplicationComponent },
        ]
    }

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DashboardRoutingModule { }
