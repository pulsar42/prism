import { OauthClient } from './oauth-client.interface';

export interface OauthDevClient extends OauthClient {
    client_secret: string;

    redirect_uri: string[];

    grant_types: string[];

    scope: string[];
}
