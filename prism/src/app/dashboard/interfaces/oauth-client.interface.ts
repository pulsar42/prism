export interface OauthClient {
    client_id: string;

    name: string;

    logo: string;

    description: string;
}
