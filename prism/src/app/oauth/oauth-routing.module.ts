import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OauthComponent } from './components/oauth/oauth.component';

const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: 'authorize' },
    { path: 'authorize', component: OauthComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OauthRoutingModule { }
