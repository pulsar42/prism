import { FeatureState, RootState } from './../../../core/state/index';
import { ClientPublic } from './../../state/oauth.state';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { OauthFetchApplication } from '../../state/oauth.actions';
import { environment } from '../../../../environments/environment';

@Component({
    selector: 'prism-oauth',
    templateUrl: './oauth.component.html',
    styleUrls: ['./oauth.component.scss']
})
export class OauthComponent {

    public validUrl: boolean = false;

    @Select((state: FeatureState) => state.oauth.application)
    public application$: Observable<ClientPublic>;

    public url: string;

    constructor(
        protected _activatedRoute: ActivatedRoute,
        protected _store: Store
    ) {
        this._activatedRoute.queryParams.subscribe((params) => {
            if (!params.client_id || !params.scope || !params.redirect_uri || !params.response_type) {
                this.validUrl = false;
            } else {
                this.validUrl = true;
            }

            if (this.validUrl) {
                // tslint:disable-next-line:max-line-length
                // this.url = `${environment.api}../oauth2/authorize${window.location.search}&jwt=${this._store.selectSnapshot((state: RootState) => state.core.jwt)}`;
                this._store.dispatch(new OauthFetchApplication(params.client_id));
            }
        });
    }

}
