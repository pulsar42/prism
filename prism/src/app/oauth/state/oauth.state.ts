import { ApiService } from './../../core/services/api.service';
import { State, Action, StateContext } from '@ngxs/store';
import { OauthFetchApplication } from './oauth.actions';
import { map } from 'rxjs/operators';

export class OauthStateModel {
    public application: ClientPublic;
}

@State<OauthStateModel>({
    name: 'oauth',
    defaults: {
        application: null
    }
})
export class OauthState {

    constructor(
        protected _api: ApiService
    ) { }

    @Action(OauthFetchApplication)
    add(ctx: StateContext<OauthStateModel>, action: OauthFetchApplication) {
        ctx.patchState({ application: null });
        return this._api.get<ClientPublic>(`clients/public/${action.clientId}`).pipe(map((client) =>
            ctx.patchState({ application: client })
        ));
    }
}


export interface ClientPublic {
    client_id: string;
    name: string;
    logo: string;
    description: string;
}
