export class OauthFetchApplication {
    static readonly type = '[Oauth] Fetch Application';
    constructor(public clientId: string) { }
}
