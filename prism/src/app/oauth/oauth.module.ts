import { NgxsModule } from '@ngxs/store';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OauthRoutingModule } from './oauth-routing.module';
import { OauthComponent } from './components/oauth/oauth.component';
import { MatIconModule, MatButtonModule } from '@angular/material';
import { OauthState } from './state/oauth.state';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [OauthComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule,
    NgxsModule.forFeature([OauthState]),
    OauthRoutingModule,
    TranslateModule
  ]
})
export class OauthModule { }
