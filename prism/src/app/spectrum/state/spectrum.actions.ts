import { SocketIOAction } from '../../core/decorators/socketio-action.decorator';

export class SpectrumAction {
    static readonly type = '[Spectrum] Add item';
    constructor(public payload: string) { }
}

/**
 * Ask the back-end to give us a spectrum auth code
 */
export class SpectrumFetchCode {
    static readonly type = '[Spectrum] fetch Code';

    constructor(public handle: string) { }
}

/**
 * Start listening for events of a given code.
 * (ie: tell me when this code is used on spectrum)
 */
@SocketIOAction()
export class SpectrumListenForCode {
    static readonly type = '[Spectrum] Listen For Code';

    /**
     * @param code the code we want to get events for
     */
    constructor(public code: string) { }
}


/**
 * Triggered when a code is claimed, we get a JWT
 */
@SocketIOAction()
export class SpectrumCodeClaimed {
    static readonly type = '[Spectrum] Code claimed';

    /**
     * @param jwt a JWT encoded base64 token for the user who claimed this
     */
    constructor(public jwt: string) { }
}
