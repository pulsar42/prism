import { ApiService } from './../../core/services/api.service';
import { State, Action, StateContext, Store } from '@ngxs/store';
import { SpectrumAction, SpectrumFetchCode, SpectrumListenForCode, SpectrumCodeClaimed } from './spectrum.actions';
import { SpectrumConfirmCode } from '../interfaces/spectrum-confirm-code.interface';
import { map, tap } from 'rxjs/operators';
import { SendSocketIOEvent } from 'ngxs-socketio-plugin';
import { SetJWT } from '../../core/state/core.actions';

export class SpectrumStateModel {
    public spectrumCode: SpectrumConfirmCode | null;
}

@State<SpectrumStateModel>({
    name: 'spectrum',
    defaults: {
        spectrumCode: null
    }
})
export class SpectrumState {

    constructor(
        protected _api: ApiService,
        protected _store: Store
    ) { }

    @Action(SpectrumFetchCode)
    public fetchSpectrumAuthCode(ctx: StateContext<SpectrumStateModel>, action: SpectrumFetchCode) {
        return this._api.post<SpectrumConfirmCode>(`spectrum/code/generate`, { handle: action.handle }).pipe(
            tap((code) => {
                if (code && code.code) {
                    this._store.dispatch(new SpectrumListenForCode(code.code));
                }
            }),
            map((code) => ctx.patchState({ spectrumCode: code })),
        );
    }

    @Action(SpectrumListenForCode)
    public listenForCode(ctx: StateContext<SpectrumStateModel>, action: SpectrumListenForCode) {
        console.log(action);
        this._store.dispatch(new SendSocketIOEvent(action, SpectrumListenForCode.type));
    }

    @Action(SpectrumCodeClaimed)
    public spectrumCodeClaimed(ctx: StateContext<SpectrumStateModel>, action: SpectrumCodeClaimed) {
        return ctx.dispatch(new SetJWT(action.jwt));
    }


}
