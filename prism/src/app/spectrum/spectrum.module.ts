import { TranslateModule } from '@ngx-translate/core';
import { SpectrumState } from './state/spectrum.state';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule, MatInputModule, MatFormFieldModule, MatButtonModule, MatIconModule, MatTooltipModule } from '@angular/material';
import { SpectrumRoutingModule } from './spectrum-routing.module';
import { RouterModule } from '@angular/router';
import { SpectrumComponent } from './components/spectrum/spectrum.component';
import { NgxsModule } from '@ngxs/store';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LocalizeRouterModule } from '@gilsdav/ngx-translate-router';
import { CountdownModule } from 'ngx-countdown';

@NgModule({
    declarations: [SpectrumComponent],
    imports: [
        CommonModule,
        SpectrumRoutingModule,
        MatCardModule,
        MatInputModule,
        MatFormFieldModule,
        MatIconModule,
        MatTooltipModule,
        FormsModule,
        ReactiveFormsModule,
        MatButtonModule,
        TranslateModule,
        LocalizeRouterModule,
        CountdownModule,
        RouterModule.forChild([
            { path: '', pathMatch: 'full', component: SpectrumComponent }
        ]),
        NgxsModule.forFeature([SpectrumState])
    ]
})
export class SpectrumModule { }
