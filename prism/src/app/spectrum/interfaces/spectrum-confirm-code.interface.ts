export interface SpectrumConfirmCode {
    code: string;
    handle: string;
    expires: Date;
}
