import { SpectrumConfirmCode } from './../../interfaces/spectrum-confirm-code.interface';
import { Component } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { RootState, FeatureState } from '../../../core/state';
import { UserIdentity } from '../../../core/interfaces/user-identity.interface';
import { Observable } from 'rxjs';
import { SpectrumFetchCode } from '../../state/spectrum.actions';
import * as copy from 'copy-to-clipboard';

@Component({
    selector: 'app-spectrum',
    templateUrl: './spectrum.component.html',
    styleUrls: ['./spectrum.component.scss']
})
export class SpectrumComponent {
    /** JWT packet of the current user */
    @Select((state: RootState) => state.core.user)
    public user$: Observable<UserIdentity>;
    /** the code (spectrum code + expiry + handle) */
    @Select((state: FeatureState) => state.spectrum.spectrumCode)
    public code$: Observable<SpectrumConfirmCode>;
    /** current handle shown in the input */
    public handle: string = '';

    public config = { template: '$!m!:$!s!' } as any;

    constructor(
        protected _store: Store
    ) {
        // When the code changes
        this.code$.subscribe((code) => {
            // If we didn't have a handle in the input, fill it.
            if (code && code.handle && !this.handle) {
                this.handle = code.handle;
            }
            if (code && code.expires) {
                this.config = { ...this.config, leftTime: (new Date(Number(code.expires) * 1000).getTime() - new Date().getTime()) / 1000 };
            }
        });
    }

    /**
     * Fetch a new code for the current handle
     */
    public fetchCode() {
        this._store.dispatch(new SpectrumFetchCode(this.handle));
    }


    public copyCode(code: string | null) {
        if (!code) { return; }
        copy(`!prism auth ${code}`);
    }
}
