import { CoreState } from './core/state/core.state';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgxsModule } from '@ngxs/store';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { NgxsRouterPluginModule } from '@ngxs/router-plugin';
import { NgxsSocketIOPluginModule } from 'ngxs-socketio-plugin';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NgxsEmitPluginModule } from '@ngxs-labs/emitter';
import { environment } from '../environments/environment';
import { MarkdownModule } from 'ngx-markdown';

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, `${environment.selfUrl}assets/i18n/`, '.json');
}


@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule.withServerTransition({ appId: 'prism' }),
        BrowserAnimationsModule,
        TransferHttpCacheModule,
        AppRoutingModule,
        HttpClientModule,
        NgxsModule.forRoot([CoreState], { developmentMode: !environment.production }),
        NgxsStoragePluginModule.forRoot({ key: ['core', 'spectrum'] }),
        NgxsRouterPluginModule.forRoot(),
        NgxsReduxDevtoolsPluginModule.forRoot(),
        NgxsSocketIOPluginModule.forRoot({
            url: environment.websocket.url,
        }),
        NgxsEmitPluginModule.forRoot(),
        NgxsLoggerPluginModule.forRoot(),
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        MarkdownModule.forRoot({ loader: HttpClient })
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
