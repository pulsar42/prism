export interface UserIdentity {
    userId: number;
    handle: string;
}
