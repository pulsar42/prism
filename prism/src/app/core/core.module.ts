import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LanguagePickerComponent } from './components/language-picker/language-picker.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { UnauthorizedInterceptor } from './interceptors/unauthorized.interceptor';

@NgModule({
    declarations: [
        LanguagePickerComponent
    ],
    imports: [
        CommonModule
    ],
    exports: [
        LanguagePickerComponent
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: UnauthorizedInterceptor,
            multi: true
        }
    ]
})
export class CoreModule { }
