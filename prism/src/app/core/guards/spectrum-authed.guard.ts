import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CanActivate } from '@angular/router';
import { Store } from '@ngxs/store';
import { RootState } from '../state';
import { map } from 'rxjs/operators';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';

@Injectable({
    providedIn: 'root'
})
export class SpectrumAuthedGuard implements CanActivate {

    constructor(
        protected _store: Store,
        protected _router: Router,
        protected _localize: LocalizeRouterService
    ) { }

    public canActivate(
        route: ActivatedRouteSnapshot,
        routerState: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
        return this._store.selectOnce((state: RootState) => state.core.user).pipe(
            map((user) => {
                if (user && user.userId) {
                    return true;
                } else {
                    // We do not have a JWT...
                    /** @TODO redirect and keep oauth params */
                    this._router.navigate(this._localize.translateRoute(['/spectrum']) as any[]);
                }
            })
        );
    }

}
