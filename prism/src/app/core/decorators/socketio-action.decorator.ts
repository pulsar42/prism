/**
* Custom action base class used to serialize to an action with a type
* when received through the backend
*/
export function SocketIOAction() {
    return function (target: any) {
        target.prototype.toJSON = function () {

            const original = this.prototype && this.prototype.toJSON ? this.prototype.toJSON() : this;

            return { ...original, type: (this as any).constructor.type };
        };
    };
}
