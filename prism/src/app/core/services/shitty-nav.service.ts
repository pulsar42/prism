import { Injectable } from '@angular/core';
import { Router, RouterEvent, NavigationError, NavigationEnd } from '@angular/router';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';

@Injectable({ providedIn: 'root' })
export class ShittyNavService {

    protected preventInfiniteLoop = false;

    constructor(
        protected router: Router,
        protected localize: LocalizeRouterService,
    ) {
        this.router
            .events
            .subscribe((event: RouterEvent) => {

                if (event instanceof NavigationError && !this.preventInfiniteLoop) {
                    if (!navigator.onLine) { return; }
                    this.preventInfiniteLoop = true;
                    return this.router.navigate([this.localize.translateRoute(event.url)]);
                }

                if (event instanceof NavigationEnd) {
                    this.preventInfiniteLoop = false;
                }

            });
    }
}
