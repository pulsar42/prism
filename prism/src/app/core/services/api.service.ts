import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Store } from '@ngxs/store';
import { RootState } from '../state';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    constructor(
        protected _http: HttpClient,
        protected _store: Store
    ) { }

    public get<T = any>(url: string) {
        return this._http.get<T>(`${environment.api}${url}`, this._headerFactory());
    }

    public post<T = any>(url: string, data: Object) {
        return this._http.post<T>(`${environment.api}${url}`, data, this._headerFactory());
    }

    public patch<T = any>(url: string, data: Object) {
        return this._http.patch<T>(`${environment.api}${url}`, data, this._headerFactory());
    }

    protected _headerFactory() {
        const jwt = this._store.selectSnapshot((state: RootState) => state.core.jwt);
        if (jwt) {
            return { headers: { 'X-Prism-auth': jwt } };
        }
    }
}
