import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { LocalizeRouterService } from '@gilsdav/ngx-translate-router';
import { Location } from '@angular/common';

@Component({
    selector: 'prism-language-picker',
    templateUrl: './language-picker.component.html',
    styleUrls: ['./language-picker.component.scss']
})
export class LanguagePickerComponent {

    constructor(
        protected translate: TranslateService,
        protected router: LocalizeRouterService,
        protected location: Location,
    ) { }

    public get lang() { return this.translate.currentLang; }

    public toggleLang() {
        let switchTo: 'fr' | 'en' = 'fr';
        if (this.translate.currentLang === 'fr') { switchTo = 'en'; }

        this.router.changeLanguage(switchTo, {}, true);

        const newPath = this.location.path(true).replace(/^\/(fr|en)/, switchTo);
        this.location.go(newPath);

    }
}
