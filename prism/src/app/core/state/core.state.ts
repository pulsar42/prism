import { ApiService } from './../services/api.service';
import { State, Action, StateContext } from '@ngxs/store';
import { CoreAction, SetJWT, GetIdentity } from './core.actions';
import { UserIdentity } from '../interfaces/user-identity.interface';
import * as jwt_decode from 'jwt-decode';
import { map } from 'rxjs/operators';

export class CoreStateModel {
    public jwt: string | null;
    public user: UserIdentity | null;
    public userIdent: { handle: string };
}

@State<CoreStateModel>({
    name: 'core',
    defaults: {
        jwt: null,
        user: null,
        userIdent: null,
    }
})
export class CoreState {

    constructor(protected _api: ApiService) { }

    @Action(SetJWT)
    public setJWT(ctx: StateContext<CoreStateModel>, action: SetJWT) {
        return ctx.patchState({ jwt: action.jwt, user: jwt_decode(action.jwt) });
    }

    @Action(GetIdentity)
    public getUserIdentity(ctx: StateContext<CoreStateModel>, action: GetIdentity) {
        return this._api.get(`users/me`).pipe(map((user) => ctx.patchState({ userIdent: user })));
    }
}
