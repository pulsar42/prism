export class CoreAction {
    static readonly type = '[Core] Add item';
    constructor(public payload: string) { }
}

export class SetJWT {
    static readonly type = '[Core] Set JWT';

    constructor(public jwt: string) { }
}

export class GetIdentity {
    static readonly type = '[Core] Get User Identity';
}
