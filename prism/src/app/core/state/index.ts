import { DashboardStateModel } from './../../dashboard/state/dashboard.state';

import { SpectrumStateModel } from './../../spectrum/state/spectrum.state';
import { CoreStateModel } from './core.state';
import { OauthStateModel } from './../../oauth/state/oauth.state';

export interface RootState {
    core: CoreStateModel;
}

export interface FeatureState extends RootState {
    spectrum: SpectrumStateModel | null;
    dashboard: DashboardStateModel | null;
    oauth: OauthStateModel | null;
}
