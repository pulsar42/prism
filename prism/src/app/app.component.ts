import { isPlatformBrowser } from '@angular/common';
import { Store } from '@ngxs/store';
import { Component, PLATFORM_ID, Inject } from '@angular/core';
import { ConnectSocketIO } from 'ngxs-socketio-plugin';

@Component({
    selector: 'app-root',
    template: `<router-outlet></router-outlet>`,
})
export class AppComponent {
    constructor(
        protected _store: Store,
        @Inject(PLATFORM_ID) platformId: Object
    ) {
        if (isPlatformBrowser(platformId)) {
            this._store.dispatch(new ConnectSocketIO());
        }
    }
}
