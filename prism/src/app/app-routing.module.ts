import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LocalizeRouterModule, LocalizeParser, LocalizeRouterSettings, ManualParserLoader } from '@gilsdav/ngx-translate-router';
import { TranslateService } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { Location } from '@angular/common';
import { SpectrumAuthedGuard } from './core/guards/spectrum-authed.guard';


const routes: Routes = [
    { path: '', loadChildren: './home/home.module#HomeModule', pathMatch: 'full' },
    { path: 'spectrum', loadChildren: './spectrum/spectrum.module#SpectrumModule' },
    { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule', canActivate: [SpectrumAuthedGuard] },
    { path: 'oauth2', loadChildren: './oauth/oauth.module#OauthModule', canActivate: [SpectrumAuthedGuard] }
];

export function LoclalizerFactory(translate, location, settings, http) {
    return new ManualParserLoader(translate, location, settings, ['en', 'fr'], '');
}

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        RouterModule.forRoot(routes),
        LocalizeRouterModule.forRoot(routes, {
            parser: {
                provide: LocalizeParser,
                useFactory: LoclalizerFactory,
                deps: [TranslateService, Location, LocalizeRouterSettings, HttpClient]
            }
        }),
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }
