export const environment = {
    production: true,
    api: 'https://api.prism.pulsar42.sc/api/v1/',
    selfUrl: 'https://prism.pulsar42.sc/',
    websocket: {
        url: 'https://api.prism.pulsar42.sc'
    }
};
